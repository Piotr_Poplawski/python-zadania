#Dwie funkcje,szufrujaca i odszyfrujaca o dane przesuniecie, mozna przesunac grubo powyzej liczby liter w alfabecie lacinskim, np o 230 liter(alfabet jest powtarzany wielokrotnie)
#by Piotr Poplawski / piopoplawski@gmail.com
#python 2.7.x
class cezar(object):

    def __init__(self,words, nr ):
        self.words = words
        self.nr = nr

    def zaszyfruj(self,val=1):
        self.val=val
        tab = []
        for y in self.words:
            y=y.lower()
            if (122>=ord(y)>=97 or ord(y)==32):
                if ( 122>=(ord(y)+self.val*self.nr)>=97):
                    tab.append(chr(ord(y)+self.val*self.nr))
                elif ord(y)==32:
                    tab.append(chr(ord(y)))
                else:
                    xxx = (ord(y)+self.val*self.nr - 97) % 26
                    tab.append(chr(xxx+97))
        if val>0:
                zzz='<-zaszyfrowane'
        else:
                zzz='<-odszyfrowane'
        return '{} {} {} {} {} {}'.format("".join(tab), zzz ,self.words ,'z przesunieciem o',self.nr, 'miejsc')

    def odszyfruj(self):
        return cezar.zaszyfruj(self,val=-1)

print cezar.zaszyfruj(cezar('a teraz POLSKA przodem',230))
print cezar.odszyfruj(cezar('w panwv lkhogw lnvkzai',230))