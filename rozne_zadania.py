#wersja python 2.7.x
#zad1 GENERATOR KODÓW POCZTOWYCH -> przyjmuje 2 stringi: "79-900" i "80-155" i zwraca listę kodów pomiędzy nimi

def zad1(kod_1,kod_2):
    m1 = kod_1.index('-')
    m2 = kod_2.index('-')
    nr_1 = kod_1[0:m1]+kod_1[m1+1:]
    nr_2 = kod_2[0:m1]+kod_2[m2+1:]
    nr_1 = int(nr_1)
    nr_2 = int(nr_2)
    tab=[]
    for i in range(nr_1+1, nr_2):
        i=str(i)
        i ='{}{}{}'.format(i[0:2],'-',i[2:])
        tab.append(i)
    return tab

print zad1('79-900','80-155')

#zad2: PODANA JEST LISTA ZAWIERAJĄCA ELEMENTY O WARTOŚCIACH 1-n. NAPISZ FUNKCJĘ KTÓRA SPRAWDZI JAKICH ELEMENTÓW BRAKUJE:
#1-n = [1,2,3,4,5,...,10]
#np. n=10
#wejście: [2,3,7,4,9], 10
#wyjście: [1,5,6,8,10]

def zad2(lista,n):
    tab=[]
    for i in range(1, n+1):
        if i not in lista:
            tab.append(i)
    return tab

print zad2([2,3,7,4,9], 10)

#zad3 NALEŻY WYGENEROWAĆ LISTĘ LICZB OD 2 DO 5.5 ZE SKOKIEM CO 0.5, DANE WYNIKOWE MUSZĄ BYĆ TYPU DECIMAL.

def zad3(start,meta,step):
    tab=[]
    x=start-step
    while x<meta:
        x+=step
        tab.append(x)
      #  tab.append('{0:.4f}'.format(x)) <-tu w formie dowolnego miejsca po przecinku, np. 4
    return tab

print zad3(2, 5.5, 0.5)


