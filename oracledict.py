"""
Works with Robot Framework.
This keyword uses cx_Oracle and connects to the database using three arguments (user, passw, address; for example: 'hr', 'hr', 'localhost:1521').
Fourth argument is a database query (for example: 'select * from table_name').
Keyword returns list containing python dictionaries where keys are the names of table columns and values are rows from columns.
For example: [{'COUNTRY_NAME': 'Argentina', 'COUNTRY_ID': 'AR'}, {'COUNTRY_NAME': 'Australia', 'COUNTRY_ID': 'AU'}]
by Piotr Poplawski / piopoplawski@gmail.com
"""

import cx_Oracle

def oracledict(user, passw, address, query):

    tab = []
    keys = []
    db = cx_Oracle.connect(user, passw, address)
    cursor = db.cursor()
    cursor.execute(query)
    desc = cursor.description
    """ Names of columns our keyword grabs from cursor description after we execute cursor on tested table """

    for i in desc:
        keys.append(i[0])
    """ Column's name is first element(index = 0) from 7-element tuple. Each column has one tuple. Names of columns(i[0]) we next append to a list named 'keys' """

    query_result = cursor.fetchall()
    """ Later, we using cursor fetchall() and grab values of all rows"""

    for value in query_result:
        dictionary = dict(zip(keys, value))
        tab.append(dictionary)
    """ When we have keys and values, our keyword generates dictionaries(one dict. = one row from all columns) """
    """ And each dictionary we append to a list named 'tab' and this is what our keyword returns at the end """
    return tab